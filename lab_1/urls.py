from django.urls import path
from .views import friend_list, index

app_name = "lab_1"
urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('friends/', friend_list, name='friend')
]
