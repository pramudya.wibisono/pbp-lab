import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MaterialApp(
    title: "ADD COVID-19 FAQ",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),
        )),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  final textStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
  final zonaHijauTextStyle =
      TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white);

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ZonaHijau"),
        backgroundColor: Colors.teal[600],
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            ListTile(
              tileColor: Colors.teal,
              title: Text(
                'ZonaHijau',
                style: GoogleFonts.sora(
                  textStyle: zonaHijauTextStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'MITOS/FAKTA',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'FUFU',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'MY FUFU',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'LIST RS',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'FAQ',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'INFOGRAFIS',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    child: Text(
                  "Add COVID-19 FAQ",
                  style: GoogleFonts.sora(
                    textStyle: textStyle,
                  ),
                )),
              ],
            ),
            height: 70,
            width: double.infinity,
            color: Color(0xffffffff),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 2,
            child: Card(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: "Write your question here..",
                              labelText: "Question..",
                              icon: Icon(
                                Icons.help,
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Pertanyaan tidak boleh kosong';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: "Write your answer here..",
                              labelText: "Answer..",
                              icon: Icon(
                                Icons.question_answer,
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Jawaban tidak boleh kosong';
                              }
                              return null;
                            },
                            maxLines: 7,
                          ),
                        ),
                        RaisedButton(
                          child: Text(
                            "Add FAQ",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.teal[600],
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              print("FAQ berhasil ditambahkan");
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
