# Jawaban Pertanyaan Lab2:

1. Apakah perbedaan antara JSON dan XML?
- JSON singkatan dari JavaScript Object Notation, sedangkan XML singkatan dari Extensible Markup Language
- Pada JSON, data disimpan seperti map atau dictionary (ada pasangan key-value), sedangkan pada XML data disimpan sebagai tree structure
- File dokumen JSON lebih ringkas dan sederhana, sedangkan file dokumen XML besar ukurannya karena terdapat struktur tag
- Pada JSON tidak ada ketentuan namespaces, komentar, ataupun metadata. Namun, XML mendukung namespaces, komentar, dan metadata
- Transfer data pada JSON lebih cepat karena ukuran file cenderung kecil dan mesin JavaScript lebih cepat penguraiannya. Akan tetapi, transmisi data pada XML lebih lambat karena penguraiannya besar

2. Apakah perbedaan antara HTML dan XML?
- HTML singkatan dari Hypertext Markup Language, sedangkan XML singkatan dari Extensible Markup Language
- HTML berfokus pada penyajian data, sedangkan XML berfokus pada transfer data
- HTML memiliki tag terbatas, sedangkan Tag XML tidak terbatas (dapat dikembangkan)
- Tag penutup pada HTML tidak strict, sedangkan pada XML tag penutupnya strict
- Tag pada HTML telah ditentukan sebelumnya, sedangkan tag pada XML tidak ditentukan sebelumnya
- HTML tidak mendukung namespaces, sedangkan XML mendukung namespaces

# Sumber:
- https://scele.cs.ui.ac.id/mod/forum/discuss.php?d=31880
- https://blogs.masterweb.com/perbedaan-xml-dan-html/