from django.core.exceptions import TooManyFieldsSent
from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=20)
    from_var = models.CharField(max_length=20)
    title = models.CharField(max_length=50)
    message = models.TextField()