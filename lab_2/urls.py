from django.urls import path
from .views import index, json, xml

app_name = 'lab_2'
urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name='xml'),
    path('json/', json, name='json')
]