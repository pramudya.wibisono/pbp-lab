from django.urls import path
from .views import add_friend, index

app_name = 'lab_3'
urlpatterns = [
    path('', index, name='index'),
    path('add/', add_friend, name='add_friend')
]