from django import forms
from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta: # Ngisi attribute yang kosong di ModelForm
        model = Friend
        fields = '__all__'