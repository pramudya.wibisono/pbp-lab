from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
