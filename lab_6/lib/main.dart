import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'COVID-19: FAQ',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.teal,
      ),
      home: const MyHomePage(title: 'ZonaHijau'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List list = [0, 0, 0, 0, 0];
  final textStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
  final zonaHijauTextStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white);

  // void _incrementCounter() {
  //   setState(() {
  //     // This call to setState tells the Flutter framework that something has
  //     // changed in this State, which causes it to rerun the build method below
  //     // so that the display can reflect the updated values. If we changed
  //     // _counter without calling setState(), then the build method would not be
  //     // called again, and so nothing would appear to happen.
  //     list[0]++;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            ListTile(
              tileColor: Colors.teal,
              title: Text(
                'ZonaHijau',
                style: GoogleFonts.sora(
                  textStyle: zonaHijauTextStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'MITOS/FAKTA',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'FUFU',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'MY FUFU',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'LIST RS',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'FAQ',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
            ListTile(
              title: Text(
                'INFOGRAFIS',
                style: GoogleFonts.sora(
                  textStyle: textStyle,
                ),
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: Column(
        // Column is also a layout widget. It takes a list of children and
        // arranges them vertically. By default, it sizes itself to fit its
        // children horizontally, and tries to be as tall as its parent.
        //
        // Invoke "debug painting" (press "p" in the console, choose the
        // "Toggle Debug Paint" action from the Flutter Inspector in Android
        // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
        // to see the wireframe for each widget.
        //
        // Column has various properties to control how it sizes itself and
        // how it positions its children. Here we use mainAxisAlignment to
        // center the children vertically; the main axis here is the vertical
        // axis because Columns are vertical (the cross axis would be
        // horizontal).
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    child: Text(
                  "COVID-19: Frequently Asked Questions",
                  style: GoogleFonts.sora(
                    textStyle: textStyle,
                  ),
                )),
              ],
            ),
            height: 70,
            width: double.infinity,
            color: Color(0xffffffff),
          ),
          FAQ(
            pertanyaan: "Can I get COVID-19 from my pets or other animals?",
            jawaban:
                'Based on the available information to date, the risk of animals spreading COVID-19 to people is considered to be low.',
            likeFunc: () {
              setState(() {
                // This call to setState tells the Flutter framework that something has
                // changed in this State, which causes it to rerun the build method below
                // so that the display can reflect the updated values. If we changed
                // _counter without calling setState(), then the build method would not be
                // called again, and so nothing would appear to happen.
                list[0]++;
              });
            },
            likes: list[0],
            textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
          ),
          FAQ(
            pertanyaan: "I have a COVID-19. How do I tell the people I was around?",
            jawaban: "If you have COVID-19, tell your close contacts you have COVID-19 so that they can quarantine at home and get tested. By letting your close contacts know they may have been exposed to COVID-19, you are helping to protect them and others within your community. You can call, text, or email your contacts. If you would like to stay anonymous, there is also an online tool that allows you to tell your contacts by sending out emails or text notifications anonymously.",
            likeFunc: () {
              setState(() {
                // This call to setState tells the Flutter framework that something has
                // changed in this State, which causes it to rerun the build method below
                // so that the display can reflect the updated values. If we changed
                // _counter without calling setState(), then the build method would not be
                // called again, and so nothing would appear to happen.
                list[1]++;
              });
            },
            likes: list[1],
            textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
          ),
          FAQ(
            pertanyaan:
                "Should I use soap and water to protect against COVID-19?",
            jawaban:
                "Handwashing is one of the best ways to protect yourself and your family from getting sick. Wash your hands often with soap and water for at least 20 seconds, especially after blowing your nose, coughing, or sneezing; going to the bathroom; and before eating or preparing food. If soap and water are not readily available, use an alcohol-based hand sanitizer with at least 60% alcohol.",
            likeFunc: () {
              setState(() {
                // This call to setState tells the Flutter framework that something has
                // changed in this State, which causes it to rerun the build method below
                // so that the display can reflect the updated values. If we changed
                // _counter without calling setState(), then the build method would not be
                // called again, and so nothing would appear to happen.
                list[2]++;
              });
            },
            likes: list[2],
            textStyle: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}

class FAQ extends StatelessWidget {
  final String pertanyaan;
  final String jawaban;
  final void Function() likeFunc;
  final int likes;
  final textStyle;

  const FAQ({
    Key? key,
    required this.pertanyaan,
    required this.jawaban,
    required this.likeFunc,
    required this.likes,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // untuk pertanyaan
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  child: Flexible(
                    child: Text(
                pertanyaan,
                textAlign: TextAlign.justify,
                style: GoogleFonts.sora(
                    textStyle: textStyle,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
                  )),
            ],
          ),
          height: 30,
          width: 370,
          color: Color(0xff41b3a3),
        ),
        Container(
          // untuk jawaban
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Flexible(
                child: Text(
                  jawaban,
                  textAlign: TextAlign.justify,
                  style: GoogleFonts.sora(
                    textStyle: textStyle,
                  ),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              )),
            ],
          ),
          height: 50,
          width: 370,
          color: Color(0xfff4f4f4),
        ),
        Container(
          // untuk button
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextButton(
                  onPressed: this.likeFunc,
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.black)),
                  child: Text(
                    this.likes.toString() + " Likes",
                    style: GoogleFonts.sora(
                      textStyle: textStyle,
                    ),
                  ))
            ],
          ),
          height: 20,
          width: 370,
          color: Color(0xfff4f4f4),
        ),
        Container(
          // untuk spacing antar FAQ
          height: 20,
          width: double.infinity,
          color: Color(0xffffffff),
        ),
      ],
    );
  }
}
